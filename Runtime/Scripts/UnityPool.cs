﻿using System;
using UnityEngine;
using UnityEngine.Pool;

namespace VSG.UnityPool.Runtime
{
    public abstract class UnityPool<TItem> : MonoBehaviour, IUnityPool<TItem> where TItem : class
    {
        protected IObjectPool<TItem> _pool;
        [Tooltip("Object that will be pooled")]
        [SerializeField] protected TItem _pooledObjectPrefab;
        [Tooltip("GameObject that will store all inactive pooled object instances")]
        [SerializeField] protected Transform _inactiveObjectsHolder;
        [Tooltip("Pool type that will be used. " +
                 "Linked version uses LinkedList to store references while Stack version will use normal List")]
        [SerializeField] protected PoolTypes _poolType = PoolTypes.Stack;
        [Tooltip("Collection checks will throw errors if we try to release an item that is already in the pool")]
        [SerializeField] protected bool _collectionChecks = true;
        [Tooltip("Maximum number of pooled object instances used by this pool")]
        [SerializeField] protected int _maxPoolSize = 10;

        [Tooltip("Object that will be pooled")]
        public TItem PooledObjectPrefab => _pooledObjectPrefab;

        [Tooltip("GameObject that will store all inactive pooled object instances")]
        public Transform InactiveObjectsHolder => _inactiveObjectsHolder;

        [Tooltip("Pool type that will be used. " +
                 "Linked version uses LinkedList to store references while Stack version will use normal List")]
        public PoolTypes PoolType => _poolType;

        [Tooltip("Collection checks will throw errors if we try to release an item that is already in the pool")]
        public bool CollectionChecks => _collectionChecks;

        [Tooltip("Maximum number of pooled object instances used by this pool")]
        public int MaxPoolSize => _maxPoolSize;

        [Tooltip("Pool object that is responsible for managing pooled items.")]
        public virtual IObjectPool<TItem> Pool =>
            _pool ??= PoolType switch
            {
                PoolTypes.Stack => new ObjectPool<TItem>(CreatePooledItem, OnTakeFromPool, OnReturnedToPool,
                    OnDestroyPoolObject, CollectionChecks, 10, MaxPoolSize),
                PoolTypes.LinkedList => new LinkedPool<TItem>(CreatePooledItem, OnTakeFromPool, OnReturnedToPool,
                    OnDestroyPoolObject, CollectionChecks, MaxPoolSize),
                _ => throw new ArgumentOutOfRangeException()
            };

        /// <summary>
        /// Instantiate a new pooled object instance.
        /// </summary>
        /// <returns>New pooled object instance></returns>
        protected virtual TItem CreatePooledItem()
        {
            Component component = CastPooledObjectToComponent(PooledObjectPrefab);
            GameObject pooledItemGameObject = Instantiate(component.gameObject, InactiveObjectsHolder);
            return pooledItemGameObject.GetComponent<TItem>();
        }

        /// <summary>
        /// Called when an item is returned to the pool using Release
        /// </summary>
        /// <param name="item">Item tha will be returned to the pool</param>
        protected virtual void OnReturnedToPool(TItem item)
        {
            Component component = CastPooledObjectToComponent(item);
            component.gameObject.SetActive(false);
            component.transform.SetParent(transform);
        }

        /// <summary>
        /// Called when an item is taken from the pool using Get
        /// </summary>
        /// <param name="item">Item tha will be taken from the pool</param>
        protected virtual void OnTakeFromPool(TItem item)
        {
            Component component = CastPooledObjectToComponent(item);
            component.gameObject.SetActive(true);
        }

        /// <summary>
        /// If the pool capacity is reached then any items returned will be destroyed.
        /// We can control what the destroy behavior does, here we destroy the GameObject.
        /// </summary>
        /// <param name="item">Item tha will be destroyed from the pool</param>
        protected virtual void OnDestroyPoolObject(TItem item)
        {
            Component component = CastPooledObjectToComponent(item);
            Destroy(component.gameObject);
        }

        /// <summary>
        /// Cast TItem pooled object to Component
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private Component CastPooledObjectToComponent(TItem item)
        {
            Component pooledObjectPrefab = PooledObjectPrefab as Component;
            if (pooledObjectPrefab != null)
            {
                return pooledObjectPrefab;
            }
            Debug.LogError(nameof(PooledObjectPrefab) + " should inherit " + nameof(Component)+ "!", this);
            return null;
        }
    }
}