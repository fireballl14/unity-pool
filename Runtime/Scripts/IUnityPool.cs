﻿using UnityEngine;
using UnityEngine.Pool;

namespace VSG.UnityPool
{
    public interface IUnityPool<TItem> where TItem : class
    {
        /// <summary>
        /// Object that will be pooled
        /// </summary>
        public TItem PooledObjectPrefab { get; }

        /// <summary>
        /// GameObject that will store all inactive pooled object instances
        /// </summary>
        public Transform InactiveObjectsHolder { get; }

        /// <summary>
        /// Pool type that will be used.
        /// Linked version uses LinkedList to store references while Stack version will use normal List.
        /// </summary>
        public PoolTypes PoolType { get; }

        /// <summary>
        /// Collection checks will throw errors if we try to release an item that is already in the pool
        /// </summary>
        public bool CollectionChecks { get; }

        /// <summary>
        /// Maximum number of pooled object instances used by this pool
        /// </summary>
        public int MaxPoolSize { get; }
        
        /// <summary>
        /// Pool object that is responsible for managing pooled items.
        /// </summary>
        public IObjectPool<TItem> Pool { get; }
    }
}